<?php


namespace App\Services;


class ProjectSubjectCrud extends AbstractCrud
{
    public function setRepositoryIdProjectId($repositoryId, $projectId)
    {
        $this->uri = str_replace('{repositoryId}', $repositoryId, $this->uri);
        $this->uri = str_replace('{projectId}', $projectId, $this->uri);
    }
}