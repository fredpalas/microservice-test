<?php


namespace App\Services;


use Mcc\ApiSdkBundle\Services\CrudAdapter;

abstract class AbstractCrud extends CrudAdapter
{
    public function setRepositoryId($repositoryId)
    {
        $this->uri = sprintf($this->uri, $repositoryId);
    }

    public function postCollection($data)
    {
        return $this->sdkClient->post($this->uri, $data, $this->className);
    }

    public function putItem($id, $data)
    {
        $uri = $this->uri.'/'.$id;

        return $this->sdkClient->put($uri, $data, $this->className);
    }
}