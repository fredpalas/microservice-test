<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JMS;

class Project
{
    /**
     * @var int
     * @JMS\Type("integer")
     * @JMS\SerializedName("id")
     */
    private $id;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     */
    private $name;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("description")
     */
    private $description;

    /**
     * @var ArrayCollection | Subject[]
     * @JMS\MaxDepth(2)
     * @JMS\Type("ArrayCollection<App\Entity\Subject>")
     */
    private $subjects;

    /**
     * @var RepositoryApp
     * @JMS\MaxDepth(1)
     * @JMS\Type("App\Entity\RepositoryApp")
     */
    private $repositoryApp;

    public function __construct()
    {
        $this->subjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Subject[]
     */
    public function getSubjects(): Collection
    {
        return $this->subjects;
    }


    public function addSubject(Subject $subject): self
    {
        if (!$this->subjects->contains($subject)) {
            $this->subjects[] = $subject;
            $subject->addProject($this);
        }

        return $this;
    }

    public function removeSubject(Subject $subject): self
    {
        if ($this->subjects->contains($subject)) {
            $this->subjects->removeElement($subject);
            $subject->removeProject($this);
        }

        return $this;
    }

    public function getRepositoryApp(): ?RepositoryApp
    {
        return $this->repositoryApp;
    }

    public function setRepositoryApp(?RepositoryApp $repositoryApp): self
    {
        $this->repositoryApp = $repositoryApp;

        return $this;
    }

    /**
     * @param ArrayCollection $subjects
     * @return Project
     */
    public function setSubjects(ArrayCollection $subjects): Project
    {
        $this->subjects = $subjects;

        return $this;
    }

    public function getSubjectById($id) {
        foreach ($this->subjects as $project) {
            if ($project->getId() === $id) {
                return $project;
            }
        }
    }
}
