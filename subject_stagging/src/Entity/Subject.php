<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JMS;

class Subject
{
    /**
     * @var int
     * @JMS\Type("integer")
     * @JMS\SerializedName("id")
     */
    private $id;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("name")
     */
    private $name;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("description")
     */
    private $description;

    /**
     * @var ArrayCollection | Subject[]
     * @JMS\MaxDepth(2)
     * @JMS\Type("ArrayCollection<App\Entity\Project>")
     */
    private $projects;

    /**
     * @var RepositoryApp
     * @JMS\MaxDepth(1)
     * @JMS\Type("App\Entity\RepositoryApp")
     */
    private $repositoryApp;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return ArrayCollection|Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
        }

        return $this;
    }

    public function getRepositoryApp(): ?RepositoryApp
    {
        return $this->repositoryApp;
    }

    public function setRepositoryApp(?RepositoryApp $repositoryApp): self
    {
        $this->repositoryApp = $repositoryApp;

        return $this;
    }

    /**
     * @param ArrayCollection|null $projects
     * @return $this
     */
    public function setProjects(?ArrayCollection $projects = null): self
    {
        $this->projects = $projects;

        return $this;
    }

    public function getProjectById($id) {
        foreach ($this->projects as $project) {
            if ($project->getId() === $id) {
                return $project;
            }
        }
    }
}
