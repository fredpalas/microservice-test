<?php


namespace App\Controller\Projects;


use App\Services\ProjectSubjectCrud;
use GuzzleHttp\Exception\GuzzleException;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProjectController
 * @package App\Controller
 * @Route("repositories/{repositoryId}/projects/{projectId}/subjects")
 */
class SubjectController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var ProjectSubjectCrud
     */
    private $subjectCrud;

    public function __construct(ProjectSubjectCrud $subjectCrud, SerializerInterface $serializer, RequestStack  $requestStack)
    {

        $this->subjectCrud = $subjectCrud;
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
    }

    /**
     * @Route(methods={"GET"})
     * @param $repositoryId
     * @param $projectId
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getIndex($repositoryId, $projectId)
    {
        $request = $this->getRequest();
        $page = $request->query->get('page', 1);
        $perPage = $request->query->get('perPage', 10);
        $this->subjectCrud->setRepositoryIdProjectId($repositoryId, $projectId);
        $result = $this->subjectCrud->getCollection($page, $perPage);


        return new JsonResponse([
            'items' => $this->serializer->toArray($result),
            'total' => $this->subjectCrud->getTotal()
        ]);
    }

    /**
     * @Route("/{id}", methods={"POST"})
     * @param $repositoryId
     * @param $projectId
     * @param $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function postSubject($repositoryId, $projectId, $id)
    {
        $request = $this->getRequest();
        $data = $request->request->all();

        $this->subjectCrud->setRepositoryIdProjectId($repositoryId, $projectId);
        $result = $this->subjectCrud->putItem($id, $data);

        return new JsonResponse($this->serializer->toArray($result));
    }

    protected function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }
}