<?php


namespace App\Controller;


use App\Services\ProjectCrud;
use GuzzleHttp\Exception\GuzzleException;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class ProjectController
 * @package App\Controller
 * @Route("repositories/{repositoryId}/projects")
 */
class ProjectController extends AbstractController
{

    /**
     * @var ProjectCrud
     */
    private $projectCrud;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(ProjectCrud $projectCrud, SerializerInterface $serializer, RequestStack  $requestStack) {

        $this->projectCrud = $projectCrud;
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
    }

    /**
     * @Route(methods={"GET"})
     * @param $repositoryId
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getIndex($repositoryId)
    {
        $request = $this->getRequest();
        $page = $request->query->get('page', 1);
        $perPage = $request->query->get('perPage', 10);
        $this->projectCrud->setRepositoryId($repositoryId);
        $result = $this->projectCrud->getCollection($page, $perPage);


        return new JsonResponse([
            'items' => $this->serializer->toArray($result),
            'total' => $this->projectCrud->getTotal()
        ]);
    }

    /**
     * @Route(methods={"POST"})
     * @param $repositoryId
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function postProjects($repositoryId)
    {
        $request = $this->getRequest();
        $data = $request->request->all();

        $this->projectCrud->setRepositoryId($repositoryId);
        $result = $this->projectCrud->postCollection($data);

        return new JsonResponse($this->serializer->toArray($result));
    }

    /**
     * @Route("/{id}", methods={"GET"})
     * @param $repositoryId
     * @param $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getProject($repositoryId, $id)
    {
        $this->projectCrud->setRepositoryId($repositoryId);
        $result = $this->projectCrud->getItem($id);


        return new JsonResponse($this->serializer->toArray($result));
    }

    /**
     * @Route("/{id}", methods={"POST"})
     * @param $repositoryId
     * @param $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function postProject($repositoryId, $id)
    {
        $request = $this->getRequest();
        $data = $request->request->all();

        $this->projectCrud->setRepositoryId($repositoryId);
        $result = $this->projectCrud->putItem($id, $data);

        return new JsonResponse($this->serializer->toArray($result));
    }

    protected function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }
}