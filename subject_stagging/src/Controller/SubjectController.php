<?php


namespace App\Controller;


use App\Services\SubjectCrud;
use GuzzleHttp\Exception\GuzzleException;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProjectController
 * @package App\Controller
 * @Route("repositories/{repositoryId}/subjects")
 */
class SubjectController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var SubjectCrud
     */
    private $subjectCrud;

    public function __construct(SubjectCrud $subjectCrud, SerializerInterface $serializer, RequestStack  $requestStack)
    {

        $this->subjectCrud = $subjectCrud;
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
    }

    /**
     * @Route(methods={"GET"})
     * @param $repositoryId
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getIndex($repositoryId)
    {
        $request = $this->getRequest();
        $page = $request->query->get('page', 1);
        $perPage = $request->query->get('perPage', 10);
        $this->subjectCrud->setRepositoryId($repositoryId);
        $result = $this->subjectCrud->getCollection($page, $perPage);


        return new JsonResponse([
            'items' => $this->serializer->toArray($result),
            'total' => $this->subjectCrud->getTotal()
        ]);
    }

    /**
     * @Route(methods={"POST"})
     * @param $repositoryId
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function postSubjects($repositoryId)
    {
        $request = $this->getRequest();
        $data = $request->request->all();

        $this->subjectCrud->setRepositoryId($repositoryId);
        $result = $this->subjectCrud->postCollection($data);

        return new JsonResponse($this->serializer->toArray($result));
    }

    /**
     * @Route("/{id}", methods={"GET"})
     * @param $repositoryId
     * @param $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getSubject($repositoryId, $id)
    {
        $this->subjectCrud->setRepositoryId($repositoryId);
        $result = $this->subjectCrud->getItem($id);


        return new JsonResponse($this->serializer->toArray($result));
    }

    /**
     * @Route("/{id}", methods={"POST"})
     * @param $repositoryId
     * @param $id
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function postSubject($repositoryId, $id)
    {
        $request = $this->getRequest();
        $data = $request->request->all();

        $this->subjectCrud->setRepositoryId($repositoryId);
        $result = $this->subjectCrud->putItem($id, $data);

        return new JsonResponse($this->serializer->toArray($result));
    }

    protected function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }
}