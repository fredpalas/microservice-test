# Microservice Stagging

Application Made with Symfony

for Crud I update an old Test Application here

[My crow Company Test](https://gitlab.com/fredpalas/mycrowcompany)

SDK Bundle for microservice connection

[Global SDK](https://gitlab.com/fredpalas/global-sdk)

## Installation 

### Requirements

[Docker](https://docs.docker.com/engine/install/)

[Docker compose](https://docs.docker.com/compose/install/)

### Instruction 
Open a terminal and execute
```bash
./first_start_from_hots.sh
```
 
 This command will build a Docker image for PHP and pull images for MariaDB and NGINX

## Usage

### Normal Access
API Crud
##### GET All subjects Paginate
###### Method GET
http://localhost/api/v1/repositories/{repositoryId}/subjects

``
repositoryId integer 
``
```
Query Params
page default 1
limit default 10
orderBy default id
order default asc
```
##### Post a Subject
###### Method POST
http://localhost/api/v1/repositories/{repositoryId}/subjects

``
repositoryId integer 
``
```json
{
    "name": "subject_test",
    "description": "some_description",
    "projects": [
        {
            "name": "some_subproject",
            "description": "some_description"
        }
    ]
}
```
##### GET a Subject
###### Method GET
http://localhost/api/v1/repositories/{repositoryId}/subjects/{id}

```
repositoryId integer 
id integer 
```
##### Update a Subject
###### Method POST
http://localhost/api/v1/repositories/{repositoryId}/subjects/{id}

```
repositoryId integer 
id integer 
```
```json
{
    "name": "subject_test",
    "description": "some_description",
    "projects": [
        {
            "id": 3,
            "name": "subproject_to_edit",
            "description": "some_description"
        },
        {
            "name": "subproject_to_add",
            "description": "some_description"
        }
    ]
}
```

##### GET All sub projects Paginate
###### Method GET
http://localhost/api/v1/repositories/{repositoryId}/subjects/{subjectId}/projects

```
repositoryId integer 
subjectId Integer
id integer 
```
```
Query Params
page default 1
limit default 10
orderBy default id
order default asc
```

##### Update a sub Project
###### Method POST
http://localhost/api/v1/repositories/{repositoryId}/subjects/{subjectId}/projects/{id}

```
repositoryId integer 
id integer 
```
```json
{
    "name": "subject_test",
    "description": "some_description"
}
```

##### GET All projects Paginate
###### Method GET
http://localhost/api/v1/repositories/{repositoryId}/projects

```
repositoryId integer 
```
```
Query Params
page default 1
limit default 10
orderBy default id
order default asc
```
##### Post a Project
###### Method POST
http://localhost/api/v1/repositories/{repositoryId}/projects

```
repositoryId integer 
```
```json
{
    "name": "project_test",
    "description": "some_description",
    "subjects": [
        {
            "name": "some_subSubject",
            "description": "some_description"
        }
    ]
}
```
##### GET a Project
###### Method GET
http://localhost/api/v1/repositories/{repositoryId}/projects/{id}

```
repositoryId integer 
id integer 
```
##### Update a Project
###### Method POST
http://localhost/api/v1/repositories/{repositoryId}/projects/{id}

```
repositoryId integer 
id integer 
```
```json
{
    "name": "project_test",
    "description": "some_description",
    "subjects": [
        {
            "id": 3,
            "name": "subSubjects_to_edit",
            "description": "some_description"
        },
        {
            "name": "subSubject_to_add",
            "description": "some_description"
        }
    ]
}
```

##### GET All sub subjects Paginate
###### Method GET
http://localhost/api/v1/repositories/{repositoryId}/projects/{projectId}/subjects

```
repositoryId integer 
subjectId Integer
id integer 
```
```
Query Params
page default 1
limit default 10
orderBy default id
order default asc
```

##### Update a sub subject
###### Method POST
http://localhost/api/v1/repositories/{repositoryId}/projects/{projectId}/subjects/{id}

```
repositoryId integer 
projectId Integer
id integer 
```
```json
{
    "name": "subject_test",
    "description": "some_description"
}
```

### Connection between services

Webserver is the main NGINX serve all the endpoint open, this connects to Stagging doing a proxy reverse.

Stagging connects to CORE using  Eight point Bundle with Guzzle HTTP Call

3 NGINX services share same network between container

Core is not expose from outside, port is close on docker-compose for testing purpose, teh application don't have a security layer.