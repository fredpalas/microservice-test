# bin/bash
git clone https://gitlab.com/fredpalas/mycrowcompany.git mycrowcompany
cd mycrowcompany
composer install
bin/console doctrine:database:create -n
bin/console doctrine:migrations:migrate -n
bin/console doctrine:fixture:load -n
cd ../subject_stagging
composer install